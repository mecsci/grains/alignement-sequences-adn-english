/*global $: false */

/*
 *
 * Javascript pour le grain alignement
 *
 */

var alignement = {
    /** Matrice coût */
    couts: {AA: 0, AT: 1, AG: 1, AC: 1, TA: 1, TT: 0, TG: 1, TC: 1, GA: 1, GT: 1, GG: 0, GC: 1, CA: 1, CT: 1, CG: 1, CC: 0},
    /** Coût d'un gap */
    coutGap: 1,
    /** Sequence 1 */
    x: ["A", "G", "T", "A", "T", "C", "T"],
    /** Sequence 2 */
    y: ["A", "G", "A", "T", "G", "C"],
    /** Liste des lettres */
    lettres: ["A", "T", "G", "C"],
    /** Etape en cours */
    etape: 0,
    /** Liste des messages */
    messages: new Array(10),
    /** Liste des instructions */
    instructions: new Array(6),
    /** Objet du canvas*/
    canvas: null,
    delta: null,
    /** Curseur */
    posX: 0,
    posY: 0,
    /** Matrice des scores*/
    scores: null,
    /** Score optimal */
    optimal: null,
    /** Liste des alignements */
    alignements: [],
    /** Liste des alignements opimaux */
    optimaux: [],
    /** Affiche un message */
    afficherMessage: function (n) {
        'use strict';
        $("#grain-alignement-messagesTitre").html(this.messages[n].titre);
        $("#grain-alignement-messagesContenu").html(this.messages[n].contenu);
    },
    /** Affichage des instructions */
    afficherInstruction: function () {
        'use strict';
        $("#grain-alignement-messagesTitre").html("Instructions");
        $("#grain-alignement-messagesContenu").html(this.instructions[this.etape].replace("COUTOPTIMAL", this.optimal));
    },
    afficherInformation: function (text) {
        'use strict';
        $("#grain-alignement-messagesTitre").html("Informations");
        $("#grain-alignement-messagesContenu").html(text);
    },
    /** Changer d'étape */
    changerEtape: function (n) {
        'use strict';
        this.etape = n;
        this.afficherInstruction();
        $("#grain-alignement-listeEtapes li").removeClass("grain-alignement-etapeActuelle");
        $("#grain-alignement-listeEtapes li:nth-child(" + n + ")").addClass("grain-alignement-etapeActuelle");
        switch (n) {
        case 1:
            this.initEtape1();
            break;
        case 2:
            this.initEtape2();
            break;
        case 3:
            this.initEtape3();
            break;
        case 4:
            this.initEtape4();
            break;
        case 5:
            this.initEtape5();
            break;
        }
    },
    /** Initilisation Etape 1 */
    initEtape1 : function () {
        'use strict';
        /*
         * Affichage des menus
         */
        var menu = $("#grain-alignement-chemins-menu");
        menu.empty();
        menu.append('<a href="javascript:alignement.next1(1)" title="Draw right"><img src="alignement/droite.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.next1(2)" title="Draw down"><img src="alignement/bas.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.next1(3)" title="Draw diagonally"><img src="alignement/diagonale.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        menu.append('<a href="javascript:alignement.retour1()" title="Cancel the last unitary path"><img src="alignement/retour.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.initEtape1()" title="Cancel the whole path"><img src="alignement/depart.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.afficherMatrice()" title="Display the matrix of substitution costs"><img src="alignement/matrice.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        /*
         * Initialisation du canvas
         */
        this.initCanvas();
        this.initScores();
        this.alignements = [];
        this.alignements.push([{x: 0, y: 0}]);
        this.posX = this.posY = 0;
        this.afficherActuel();
        this.afficherAlignements();
    },
    next1: function (dir) {
        'use strict';
        var x = this.posX,
            y = this.posY,
            xx = (dir === 2) ? x : x + 1,
            yy = (dir === 1) ? y : y + 1;
        if (xx <= this.x.length && yy <= this.y.length) {
            this.afficherTrait(x, y, xx, yy);
            this.afficherParent(x, y);
            this.posX = xx;
            this.posY = yy;
            this.scores[xx][yy] = this.scores[x][y] + this.calculCout(xx, yy, dir);
            this.alignements[0].push({x: xx, y: yy});
            this.afficherActuel();
            this.afficherAlignements();
            this.actualiserGrille();
            switch (dir) {
            case 1:
                this.afficherInformation("Insertion of a <b>gap</b> in the first sequence (in rows): the cost is " + this.calculCout(xx, yy, dir));
                break;
            case 2:
                this.afficherInformation("Insertion of a <b>gap</b> in the second sequence (in columns). the cost is: " + this.calculCout(xx, yy, dir));
                break;
            case 3:
                this.afficherInformation("Aligning character " + this.x[xx - 1] + " with character " + this.y[yy - 1] + ": the cost is " + this.calculCout(xx, yy, dir));
                break;
            }
        }
        if (xx === this.x.length && yy === this.y.length) {
            $("#etape2").show();
        }
    },
    /** Retour */
    retour1: function () {
        'use strict';
        var j,
            alig = this.alignements[0];
        if (alig.length > 1) {
            this.canvas.removeLayerGroup("scores");
            this.canvas.removeLayerGroup("chemin");
            this.alignements[0].pop();
            for (j = 0; j < alig.length - 1; j = j + 1) {
                this.afficherParent(alig[j].x, alig[j].y);
                this.afficherTrait(alig[j].x, alig[j].y, alig[j + 1].x, alig[j + 1].y);
            }
            this.posX = alig[j].x;
            this.posY = alig[j].y;
            this.afficherActuel();
            this.actualiserGrille();
            this.afficherAlignements();
        }
    },
    initEtape2: function () {
        'use strict';
        /*
         * Affichage des menus
         */
        var menu = $("#grain-alignement-chemins-menu");
        menu.empty();
        menu.append('<a href="javascript:alignement.next2(1)"><img src="alignement/droite.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.next2(2)"><img src="alignement/bas.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.next2(3)"><img src="alignement/diagonale.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        menu.append('<a href="javascript:alignement.retour2()"><img src="alignement/retour.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.initEtape2()"><img src="alignement/depart.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.afficherMatrice()"><img src="alignement/matrice.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        /*
         * Initialisation du canvas
         */
        this.initCanvas();
        this.initScores();
        this.alignements = [];
        this.alignements.push([{x: 0, y: 0}]);
        this.posX = this.posY = 0;
        this.afficherActuel();
        this.afficherAlignements();
    },
    next2: function (dir) {
        'use strict';
        var x = this.posX,
            y = this.posY,
            xx = (dir === 2) ? x : x + 1,
            yy = (dir === 1) ? y : y + 1,
            score;
        if (xx <= this.x.length && yy <= this.y.length) {
            score = this.scores[x][y] + this.calculCout(xx, yy, dir);
            if (this.scores[xx][yy] === null || this.scores[xx][yy] >= score) {
                this.afficherTrait(x, y, xx, yy);
                this.afficherParent(x, y);
                this.posX = xx;
                this.posY = yy;
                this.scores[xx][yy] = this.scores[x][y] + this.calculCout(xx, yy, dir);
                this.alignements[0].push({x: xx, y: yy});
                this.afficherActuel();
                this.afficherAlignements();
                this.actualiserGrille();
                switch (dir) {
                case 1:
                    this.afficherInformation("Insertion of a <b>gap</b> in the first sequence (in rows): the cost is " + this.calculCout(xx, yy, dir) + ".");
                    break;
                case 2:
                    this.afficherInformation("Insertion of a <b>gap</b> in the second sequence (in columns).");
                    break;
                case 3:
                    this.afficherInformation("Aligning character " + this.x[xx - 1] + " with character " + this.y[yy - 1] + ": the <b>cost</b> is " + this.calculCout(xx, yy, dir) + ".");
                    break;
                }
                if (xx === this.x.length && yy === this.y.length) {
                    if (score === this.optimal) {
                        this.afficherInformation("Congratulations. You got an optimal alignment!");
                        $("#etape3").show();
                    } else {
                        this.afficherInformation("The cost of your alignement (" + score + ") is higher than those of the optimal one (" + this.optimal + ").");
                    }
                }
            } else {
                this.afficherInformation("The cost attached to this node is lower than the cost of the path you are drawing.<br/> It is therefore useless to carry on this path.");
            }
        }
    },
    retour2: function () {
        'use strict';
        var i, j;
        this.retour1();
        for (i = 0; i <= this.x.length; i = i + 1) {
            for (j = 0; j <= this.y.length; j = j + 1) {
                if (this.scores[i][j] !== null) {
                    this.afficherScore(i, j);
                }
            }
        }
    },
    initEtape3 : function () {
        'use strict';
        /*
         * Affichage des menus
         */
        var menu = $("#grain-alignement-chemins-menu");
        menu.empty();
        menu.append('<a href="javascript:alignement.next3(-2)"><img src="alignement/debut.png" width="20" height="20" border="0" title="Reset" /></a> ');
        menu.append('<a href="javascript:alignement.next3(-1)"><img src="alignement/recul.png" width="20" height="20" border="0" title="Go back" /></a> ');
        menu.append('<a href="javascript:alignement.next3(1)"><img src="alignement/avance.png" width="20" height="20" border="0" title="Play the algorithm step by step" /></a> ');
        menu.append('<a href="javascript:alignement.next3(2)"><img src="alignement/fin.png" width="20" height="20" border="0" title="Play the algorithm on the whole grid" /></a> ');
        menu.append(' &nbsp; ');
        menu.append('<a href="javascript:alignement.afficherMatrice()"><img src="alignement/matrice.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        /*
         * Initialisation du canvas
         */
        this.initCanvas();
        this.initScores();
        this.alignements = [];
        this.alignements.push([{x: 0, y: 0}]);
        this.posX = this.posY = 0;
        this.afficherActuel();
        this.afficherAlignements();
    },
    next3: function (n) {
        'use strict';
        if (n > 0) {
            this.afficherInformation("Compute the optimal cost of the node from the computed costs that are attached to the three incoming nodes.");
            do {
                if (this.posX !== this.x.length || this.posY !== this.y.length) {
                    this.avance3();
                } else {
                    n = 0;
                    $("#etape4").show();
                }
            } while (n === 2);
        }
        if (n < 0) {
            do {
                if (this.posX !== 0 || this.posY !== 0) {
                    this.recul3();
                } else {
                    n = 0;
                }
            } while (n === -2);
            this.canvas.removeLayerGroup("chemin");
            this.canvas.removeLayer("score_" + this.posX + "_" + this.posY);
            this.afficherActuel();
        }
        this.canvas.removeLayerGroup("couts");
        this.afficherCouts();
        this.actualiserGrille();
    },
    avance3: function () {
        'use strict';
        var x = this.posX,
            y = this.posY,
            score,
            s;
        x = x - 1;
        y = y + 1;
        if (x < 0 || y > this.y.length) { x = y + x + 1; y = 0; }
        if (x > this.x.length) { y = x - this.x.length; x = this.x.length; }
        this.posX = x;
        this.posY = y;
        this.canvas.removeLayerGroup("chemin");
        if (x > 0) {
            s = this.scores[x - 1][y] + this.calculCout(x, y, 1);
            if (score === undefined || score > s) {
                score = s;
            }
        }
        if (y > 0) {
            s = this.scores[x][y - 1] + this.calculCout(x, y, 2);
            if (score === undefined || score > s) {
                score = s;
            }
        }
        if (x > 0 && y > 0) {
            s = this.scores[x - 1][y - 1] + this.calculCout(x, y, 3);
            if (score === undefined || score > s) {
                score = s;
            }
        }
        this.scores[x][y] = score;
        this.afficherActuel();
    },
    recul3: function () {
        'use strict';
        var x = this.posX,
            y = this.posY;
        this.canvas.removeLayer("score_" + x + "_" + y);
        x = x + 1;
        y = y - 1;
        if (x > this.x.length || y < 0) { x = y + x - 1 - this.y.length; y = this.y.length; }
        if (x < 0) { y = this.y.length + x; x = 0; }
        this.posX = x;
        this.posY = y;
    },
    initEtape4 : function () {
        'use strict';
        /*
         * Affichage des menus
         */
        var menu = $("#grain-alignement-chemins-menu");
        menu.empty();
        menu.append('<a href="javascript:alignement.next4(1)"><img src="alignement/gauche.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.next4(2)"><img src="alignement/haut.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.next4(3)"><img src="alignement/diagonale2.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        menu.append('<a href="javascript:alignement.retour4()"><img src="alignement/retour.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.initEtape4()"><img src="alignement/depart.png" width="20" height="20" border="0" /></a> ');
        menu.append('<a href="javascript:alignement.afficherMatrice()"><img src="alignement/matrice.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        /*
         * Initialisation du canvas
         */
        this.initCanvas();
        this.initScores();
        this.alignements = [];
        this.alignements.push([{x: this.x.length, y: this.y.length}]);
        this.posX = this.posY = 0;
        this.afficherActuel();
        this.afficherAlignements();
        // Affichage des scores
        while (this.posX !== this.x.length || this.posY !== this.y.length) {
            this.avance3();
        }
        this.afficherCouts();
        this.actualiserGrille();
    },
    next4: function (dir) {
        'use strict';
        var x = this.posX,
            y = this.posY,
            xx = (dir === 2) ? x : x - 1,
            yy = (dir === 1) ? y : y - 1;
        if (xx >= 0 && yy >= 0) {
            if ((this.scores[x][y] - this.calculCout(x, y, dir)).toFixed(1) === this.scores[xx][yy].toFixed(1)) {
                this.afficherTrait(x, y, xx, yy);
                this.afficherParent(x, y);
                this.posX = xx;
                this.posY = yy;
                this.alignements[0].unshift({x: xx, y: yy});
                this.afficherActuel();
                this.canvas.removeLayerGroup("couts");
                this.afficherCouts();
                this.afficherAlignements();
                this.actualiserGrille();
                this.afficherInformation("");
                if (xx === 0 && yy === 0) {
                    this.afficherInformation("Congratulations. You got an optimal alignment!");
                    $("#etape5").show();
                }
            } else {
                this.afficherInformation("The node you selected cannot be part of the optimal path because the transition cost is too high. " + this.scores[xx][yy] + " + " + this.calculCout(x, y, dir) + " > " + this.scores[x][y]);
            }
        }
    },
    retour4: function () {
        'use strict';
        var j,
            alig = this.alignements[0];
        if (alig.length > 1) {
            this.canvas.removeLayerGroup("chemin");
            alig.shift();
            for (j = 1; j < alig.length; j = j + 1) {
                this.afficherParent(alig[j].x, alig[j].y);
                this.afficherTrait(alig[j].x, alig[j].y, alig[j - 1].x, alig[j - 1].y);
            }
            this.posX = alig[0].x;
            this.posY = alig[0].y;
            this.afficherActuel();
            this.canvas.removeLayerGroup("couts");
            this.afficherCouts();
            this.actualiserGrille();
            this.afficherAlignements();
        }
    },
    initEtape5 : function () {
        'use strict';
        /*
         * Affichage des menus
         */
        var menu = $("#grain-alignement-chemins-menu"), i;
        menu.empty();
        menu.append('<a href="javascript:alignement.initEtape5()"><img src="alignement/debut.png" width="20" height="20" border="0" title="Reset" /></a> ');
        menu.append('<a href="javascript:alignement.next5(-1)"><img src="alignement/recul.png" width="20" height="20" border="0" title="Go back" /></a> ');
        menu.append('<a href="javascript:alignement.next5(1)"><img src="alignement/avance.png" width="20" height="20" border="0" title="Play the algorithm step by step" /></a> ');
        menu.append('<a href="javascript:alignement.next5(2)"><img src="alignement/fin.png" width="20" height="20" border="0" title="Play the algorithm on the whole grid" /></a> ');
        menu.append(' &nbsp; ');
        menu.append('<a href="javascript:alignement.afficherMatrice()"><img src="alignement/matrice.png" width="20" height="20" border="0" /></a> ');
        menu.append(' &nbsp; ');
        /*
         * Initialisation du canvas
         */
        this.initCanvas();
        this.initScores();
        this.posX = this.posY = 0;
        this.afficherActuel();
        // Affichage des scores
        while (this.posX !== this.x.length || this.posY !== this.y.length) {
            this.avance3();
        }
        this.actualiserGrille();
        /*
         * Détection des chemins optimaux
         */
        this.optimaux = this.detectionOptimaux([{x: this.x.length, y: this.y.length}]);
        this.afficherCouts();
        this.actualiserGrille();
        this.alignements = [];
        this.posM = 0;
        for (i = 0; i < this.optimaux.length; i = i + 1) {
            this.alignements.push([{x: this.x.length, y: this.y.length}]);
            this.posM = Math.max(this.posM, this.optimaux[i].length);
        }
        this.afficherAlignements();
        this.pos5 = 0;
    },
    detectionOptimaux: function (opt) {
        'use strict';
        var x = opt[opt.length - 1].x,
            y = opt[opt.length - 1].y,
            optimaux = [],
            dir,
            xx,
            yy,
            opt2;
        if (x === 0 && y === 0) {
            return [opt];
        }
        for (dir = 1; dir <= 3; dir = dir + 1) {
            xx = (dir === 2) ? x : x - 1;
            yy = (dir === 1) ? y : y - 1;
            if (xx >= 0 && yy >= 0 && (this.scores[x][y] - this.calculCout(x, y, dir)).toFixed(1) === this.scores[xx][yy].toFixed(1)) {
                opt2 = opt.slice();
                opt2.push({x: xx, y: yy});
                optimaux = optimaux.concat(this.detectionOptimaux(opt2));
            }
        }
        return optimaux;
    },
    pos5: 0,
    posM: 0,
    next5: function (n) {
        'use strict';
        if (n > 0) {
            do {
                if (this.pos5 < this.posM - 1) {
                    this.avance5();
                } else {
                    n = 0;
                }
            } while (n === 2);
        }
        if (n < 0) {
            if (this.pos5 <= 1) {
                this.initEtape5();
            } else {
                this.recul5();
            }
        }
        this.afficherAlignements();
        this.actualiserGrille();
        this.afficherInformation("Computation of the optimal node or nodes among the three nodes that ends or end at the current node.");
    },
    avance5: function () {
        'use strict';
        var i, x, y, yy, xx, m, j, opt;
        this.pos5 = this.pos5 + 1;
        this.canvas.removeLayerGroup("couts");
        for (i = 0; i < this.optimaux.length; i = i + 1) {
            opt = this.optimaux[i];
            if (this.pos5 < opt.length) {
                x = opt[this.pos5 - 1].x;
                y = opt[this.pos5 - 1].y;
                xx = opt[this.pos5].x;
                yy = opt[this.pos5].y;
                m = Math.min(this.pos5, opt.length - 1);
                for (j = 1; j <= m; j = j + 1) {
                    this.canvas.removeLayer("score_" + opt[j - 1].x + "_" + opt[j - 1].y);
                    this.afficherParent(opt[j - 1].x, opt[j - 1].y);
                    this.afficherTrait(opt[j].x, opt[j].y, opt[j - 1].x, opt[j - 1].y);
                }
                this.afficherTrait(x, y, xx, yy);
                this.canvas.removeLayer("score_" + x + "_" + y);
                this.afficherParent(x, y);
                this.alignements[i].unshift({x: xx, y: yy});
                this.afficherCouts(xx, yy);
            }
        }
        for (i = 0; i < this.optimaux.length; i = i + 1) {
            opt = this.optimaux[i];
            if (this.pos5 < opt.length) {
                xx = opt[this.pos5].x;
                yy = opt[this.pos5].y;
                this.canvas.removeLayer("score_" + xx + "_" + yy);
                this.afficherActuel(xx, yy);
            }
        }
    },
    recul5: function () {
        'use strict';
        var i, j, opt, m, xx, yy;
        this.pos5 = this.pos5 - 1;
        this.canvas.removeLayerGroup("chemin");
        this.canvas.removeLayerGroup("couts");
        for (i = 0; i < this.optimaux.length; i = i + 1) {
            opt = this.optimaux[i];
            m = Math.min(this.pos5, opt.length - 1);
            for (j = 1; j <= m; j = j + 1) {
                this.canvas.removeLayer("score_" + opt[j - 1].x + "_" + opt[j - 1].y);
                this.afficherParent(opt[j - 1].x, opt[j - 1].y);
                this.afficherTrait(opt[j].x, opt[j].y, opt[j - 1].x, opt[j - 1].y);
            }
            if (this.pos5 < opt.length - 1) {
                xx = opt[this.pos5].x;
                yy = opt[this.pos5].y;
                this.alignements[i].shift();
                this.afficherCouts(xx, yy);
            }
        }
        for (i = 0; i < this.optimaux.length; i = i + 1) {
            opt = this.optimaux[i];
            if (this.pos5 < opt.length) {
                xx = opt[this.pos5].x;
                yy = opt[this.pos5].y;
                this.canvas.removeLayer("score_" + xx + "_" + yy);
                this.afficherActuel(xx, yy);
            }
        }
    },
    call5: function () {
        'use strict';
        var i = $(this).attr("alig");
        $("#grain-alignement-listeAlignements table").removeClass("grain-alignement-selection");
        $('#grain-alignement-listeAlignements table[alig="' + i + '"]').addClass("grain-alignement-selection");
        alignement.afficher5(i);
    },
    afficher5: function (i) {
        'use strict';
        var m, opt, j, xx, yy;
        this.canvas.removeLayerGroup("chemin");
        this.canvas.removeLayerGroup("couts");
        opt = this.optimaux[i];
        m = Math.min(this.pos5, opt.length - 1);
        for (j = 1; j <= m; j = j + 1) {
            this.canvas.removeLayer("score_" + opt[j - 1].x + "_" + opt[j - 1].y);
            this.afficherParent(opt[j - 1].x, opt[j - 1].y);
            this.afficherTrait(opt[j].x, opt[j].y, opt[j - 1].x, opt[j - 1].y);
        }
        xx = opt[m].x;
        yy = opt[m].y;
        this.afficherCouts(xx, yy);
        this.canvas.removeLayer("score_" + xx + "_" + yy);
        this.afficherActuel(xx, yy);
        this.actualiserGrille();
        this.afficherInformation("Computation of the optimal node or nodes among the three nodes that ends or end at the current node.");
    },
    /**
     * Initialisation du grain
     */
    init: function () {
        'use strict';
        /*
         * Génération de la matrice des scores
         */
        var i, j, obj2,
            obj = $(" #grain-alignement-matrice table");
        obj.empty();
        obj2 = $('<tr></tr>');
        obj2.append("<th></th>");
        for (i = 0; i < this.lettres.length; i = i + 1) {
            obj2.append('<th>' + this.lettres[i] + '</th>');
        }
        obj.append(obj2);
        for (i = 0; i < this.lettres.length; i = i + 1) {
            obj2 = $('<tr></tr>');
            obj2.append('<th>' + this.lettres[i] + '</th>');
            for (j = 0; j < this.lettres.length; j = j + 1) {
                obj2.append('<td>' + this.couts[this.lettres[i] + this.lettres[j]] + '</td>');
            }
            obj.append(obj2);
        }
        $("#grain-alignement-matrice").addClass("grain-alignement-type-" + this.lettres.length);
        /*
         * Affichage du coût optimal
         */
        this.initEtape3();
        this.next3(2);
        for (i = 2; i <= 5; i = i + 1) {
            $("li#etape" + i).hide();
        }
        this.optimal = this.scores[this.x.length][this.y.length];
        $("#grain-alignement-optimal .grain-alignement-contenu").html(this.optimal.toFixed(1));
    },
    /**
     * Initialisation du canvas
     */
    initCanvas: function () {
        'use strict';
        var i, j, couleur, h, w;
        /*
         * Parametres du canvas
         */
        this.canvas = $("#grain-alignement-canvas");
        this.canvas.clearCanvas();
        this.canvas.removeLayers();
        h = this.canvas.innerHeight() - 60;
        w = this.canvas.innerWidth() - 60;
        this.delta = Math.min(w / (this.x.length), h / (this.y.length));
        /*
         * Affichage de la grille
         */
        couleur = this.coutGap === 0 ? "#0C0" : "red";
        //Horizontal
        for (i = 0; i <= this.x.length; i = i + 1) {
            this.afficherGrille(i, 0, i, this.y.length, couleur);
        }
        for (i = 0; i < this.x.length; i = i + 1) {
            this.canvas.drawText({layer: true, group: "lettres", fillStyle: "red", fontSize: "18px", fontFamily: "Arial", x: 40 + (i + 0.5) * this.delta, y: 14, text: this.x[i]});
        }

        //Vertical
        for (i = 0; i <= this.y.length; i = i + 1) {
            this.afficherGrille(0, i, this.x.length, i, couleur);
        }
        for (i = 0; i < this.y.length; i = i + 1) {
            this.canvas.drawText({layer: true, group: "lettres", fillStyle: "green", fontSize: "18px", fontFamily: "Arial", y: 40 + (i + 0.5) * this.delta, x: 12, text: this.y[i]});
        }
        for (i = 1; i <= this.x.length; i = i + 1) {
            for (j = 1; j <= this.y.length; j = j + 1) {
                couleur = this.calculCout(i, j, 3) === 0 ? "#0C0" : "red";
                this.afficherGrille(i - 1, j - 1, i, j, couleur);
            }
        }

        //Cercles
        for (i = 0; i <= this.x.length; i = i + 1) {
            for (j = 0; j <= this.y.length; j = j + 1) {
                this.canvas.drawEllipse({layer: true, group: "bulles", strokeStyle: "#000", strokeWidth: 1, fillStyle: "#FFF", x: 40 + i * this.delta, y: 40 + j * this.delta, width: 21, height: 21});
            }
        }
    },
    /**
     * Initialisation de la matrice de score
     */
    initScores: function () {
        'use strict';
        var i, j;
        this.scores = [];
        for (i = 0; i <= this.x.length; i = i + 1) {
            this.scores[i] = [];
            for (j = 0; j <= this.y.length; j = j + 1) {
                this.scores[i][j] = null;
            }
        }
        this.scores[0][0] = 0;
    },
    afficherTrait: function (x, y, xx, yy, couts) {
        'use strict';
        var couleur = couts ? "#F0F" : "#00F",
            groupe = couts ? "couts" : "chemin";
        this.canvas.drawLine({layer: true, group: groupe, index: 0, strokeStyle: couleur, strokeWidth: 3, x1: 40 + x * this.delta, y1: 40 + y * this.delta, x2: 40 + xx * this.delta, y2: 40 + yy * this.delta});
    },
    afficherCouts: function (x, y) {
        'use strict';
        if (x === undefined) {
            x = this.posX;
        }
        if (y === undefined) {
            y = this.posY;
        }
        if (x > 0) {
            this.afficherTrait(x - 1, y, x, y, true);
            this.afficherScore(x - 0.55, y - 0.15, this.calculCout(x, y, 1));
        }
        if (y > 0) {
            this.afficherTrait(x, y - 1, x, y, true);
            this.afficherScore(x + 0.15, y - 0.55, this.calculCout(x, y, 2));
        }
        if (x > 0 && y > 0) {
            this.afficherTrait(x - 1, y - 1, x, y, true);
            this.afficherScore(x - 0.35, y - 0.55, this.calculCout(x, y, 3));
        }
    },
    actualiserGrille: function () {
        'use strict';
        this.canvas.getLayers(function (layer) {
            if (layer.group === "grille") {
                alignement.canvas.moveLayer(layer, 0);
            }
        });
        this.canvas.clearCanvas().drawLayers();
    },
    afficherParent: function (x, y) {
        'use strict';
        this.canvas.drawEllipse({layer: true, group: "chemin", strokeStyle: "#00F", strokeWidth: 2, fillStyle: "#CC0", x: 40 + x * this.delta, y: 40 + y * this.delta, width: 21, height: 21});
        this.afficherScore(x, y);
    },
    afficherActuel: function (x, y) {
        'use strict';
        if (x === undefined) {
            x = this.posX;
        }
        if (y === undefined) {
            y = this.posY;
        }
        this.canvas.drawEllipse({layer: true, group: "chemin", strokeStyle: "#000", strokeWidth: 1, fillStyle: "#FF0", x: 40 + x * this.delta, y: 40 + y * this.delta, width: 21, height: 21});
        this.afficherScore(x, y);
    },
    afficherScore: function (x, y, s) {
        'use strict';
        var g = "couts",
            name = "score_" + x + "_" + y;
        if (s === undefined) {
            s = this.scores[x][y];
            g = "scores";
        }
        this.canvas.drawText({layer: true, name: name, group: g, fillStyle: "#000", fontSize: "11px", fontFamily: "Arial", x: 40 + x * this.delta, y: 40 + y * this.delta, text: s.toFixed(1)});
    },
    afficherGrille: function (x, y, xx, yy, couleur) {
        'use strict';
        this.canvas.drawLine({layer: true, group: "grille", strokeStyle: couleur, strokeWidth: 1, x1 : 40 + x * this.delta, y1: 40 + y * this.delta, x2: 40 + xx * this.delta, y2: 40 + yy * this.delta});
    },
    afficherAlignements: function () {
        'use strict';
        var i, j, l, ll, alig, obj2,
            obj = $("#grain-alignement-listeAlignements").empty();
        for (i = 0; i < this.alignements.length; i = i + 1) {
            alig = this.alignements[i];
            l = "";
            ll = "";
            for (j = 1; j < alig.length; j = j + 1) {
                l  += '<td>' + ((alig[j].y === alig[j - 1].y) ? "-" : this.y[alig[j].y - 1]) + '</td>';
                ll += '<td>' + ((alig[j].x === alig[j - 1].x) ? "-" : this.x[alig[j].x - 1]) + '</td>';
            }
            obj2 = $('<table><tr>' + l + '</tr><tr>' + ll + '</tr></table>');
            if (this.etape === 5) {
                obj2.click(alignement.call5);
                obj2.attr("alig", i);
                obj2.addClass("grain-alignement-pointer");
            }
            obj.append(obj2);
        }
    },
    afficherMatrice: function () {
        'use strict';
        $(" #grain-alignement-matrice ").toggle();
    },
    /** Calcul du cout pour un déplacement */
    calculCout: function (x, y, dir) {
        'use strict';
        if (dir !== 3) {
            return this.coutGap;
        }
        return this.couts[this.x[x - 1] + this.y[y - 1]];
    }
};

/*
 * Liste des messages
 */
alignement.messages[1] = {titre: "Current alignment", contenu: "The current alignment correspond to the path you are drawing.<br/> \"-\" represents the insertion of a gap in the sequence."};
alignement.messages[2] = {titre: "Optimal cost", contenu: "The cost of a path is the sum of transition costs from one node to the next ; in another words, the sum of substitution and gap insertion costs."};

/*
 * Liste des instructions
 */
alignement.instructions[1] = "<strong>Objective</strong> : to draw a path as an alignment of the two sequences and compare its cost with the optimal cost<br><br>"
    + "<strong>Explanation</strong> : the path starts from the upper left node of the grid. To start the path, select one of the three possible directions by clicking on one of the three arrows in the upper part of the <strong>Paths</strong>. The path you draw determines a partial alignment that is displayed in this <strong>Current alignment</strong> frame.<br>"
    + "The last selected node is highlighted in yellow together with the value of the corresponding partial alignment.</br>"
    + "When you reach the terminal node (in the lower right corner of the grid), you have obtained an alignment of the two sequences.</br>"
    + "Compare the cost of this alignment with the actual optimal cost that appears in the frame <strong>Optimal cost</strong>.<br>"
    + "You may come back over the last selected node by clicking on the orange arrow so as to try obtaining a better alignment. You may also restart from the beginning by clicking on the double orange arrow.";
alignement.instructions[2] = "<strong>Objective</strong> : To draw a path with the optimal cost (here, 3).<br><br>"
    + "<strong>A trick</strong> : to help you obtaining an optimal alignment, the cost of the selected path is displayed on the end node and it is still displayed if you decide to come back on the previous node (clicking on the orange arrow) to select another direction. You can thus easily see if you have the possibility to improve the cost you previously got.";
alignement.instructions[3] = "<strong>Objective</strong> : to follow step by step the first phase of the iterative algorithm.<br><br>"
    + "<strong>Explanations</strong> : Click on the green arrows in the <strong>Paths</strong> frame to follow the execution of the algorithm, which computes the optimal sub-paths that end at each node of the grid.";
alignement.instructions[4] = "<strong>Objective</strong> : To trace back the optimal alignments starting from the end node.<br><br>"
    + "<strong>Explanations</strong>: Start from the end node (the lower right node of the grid) and go back to the start node by selecting (with the arrows in the <strong>Paths</strong> frame) the preceding node for the sum of the cost and of the transition cost is in accordance with the optimality of the current node.";
alignement.instructions[5] = "<strong>Objective</strong> : To follow step by step the second pass of the algorithm.<br><br>"
    + "<strong>Explanations</strong> : Click on the green arrows in the frame <strong>Paths</strong> to study the second phase of the algorithm.<br>"
    + "Follow with particular attention the steps when several nodes can be chosen because that means several optimal alignments (with the same optimal cost) do exist.";
